import './App.css';
import Unity, { UnityContext } from "react-unity-webgl";
import React, { useState, useEffect } from "react";
import Payload from "./erik-tst.json";


const unityContext = new UnityContext({
  loaderUrl: "ReactTestBuilt/Build/ReactTestBuilt.loader.js",
  dataUrl: "ReactTestBuilt/Build/ReactTestBuilt.data",
  frameworkUrl: "ReactTestBuilt/Build/ReactTestBuilt.framework.js",
  codeUrl: "ReactTestBuilt/Build/ReactTestBuilt.wasm",
});

const timer = ms => new Promise(res => setTimeout(res, ms));

function App() {
  var lines = [];
  const [message, setMessage] = useState("");
  const [files, setFiles] = useState("");

  const handleChange = e => {
    const fileReader = new FileReader();
    fileReader.readAsText(e.target.files[0], "UTF-8");
    fileReader.onload = e => {
      setFiles(e.target.result);
    };
  };

  function postMessage(){
    unityContext.send("Interface", "AddDebugText", message);
    //unityContext.setFullscreen(true);
  }

  function sendFile(){
    lines = files.split('\n');

    var count = 0;

    function sendMess(){
      if(count < lines.length){
        unityContext.send("Interface", "IWMUpdate", lines[count]);
        console.log(lines[count]);
        count++;
      }
    }
    setInterval(sendMess, 2000);
  }

  function onChange(event) {
    setMessage(event.target.value);
  }

  useEffect(function () {
    unityContext.on("canvas", function (canvas) {
      canvas.width = 1080;
      canvas.height = 720;
    });
  }, []);

  
  return (
    <div className="App">
      <h1>Hello</h1>
      <Unity unityContext={unityContext} 
            matchWebGLToCanvasSize={false}
            style={{ width: "90%", height: "70%" }}/>
      <br />
      <h1>Type in debug text and press "Send debug text" to display on screen</h1>
      <br/>
      <label>
        Input:
      </label>
      <br />
      <textarea type="textarea" value={message} onChange={onChange} rows={10}/>       
      <br />
      <button onClick={postMessage}>Send debug text</button>

      <h1>Upload Json file of object to send to Unity Build</h1>

      <input type="file" onChange={handleChange} />
      <br />
      <br />
      <button onClick={sendFile}>Send file</button>
    </div>
  );
}


export default App;
