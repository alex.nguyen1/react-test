import React from 'react';
import Unity, { UnityContext } from "react-unity-webgl";
import App from './App';


const unityContext = new UnityContext({
  loaderUrl: "Build/ReactTestBuilt.loader.js",
  dataUrl: "Build/ReactTestBuilt.data",
  frameworkUrl: "Build/ReactTestBuilt.framework.js",
  codeUrl: "Build/ReactTestBuilt.wasm",
});




class WebGLForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {    this.setState({value: event.target.value});  }
    handleSubmit(event) {
        postMessage(this.state.value);
    }

    render() {
        return (
        <div>
            <form onSubmit={this.handleSubmit}>       
                <label>
                    Name:
                    <input type="text" value={this.state.value} onChange={this.handleChange} />
                </label>
                <input type="submit" value="Submit" />
            </form>
        </div>

        );
    }
}

export default WebGLForm;