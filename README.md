# React Testing App

The program now allow you to send message to debug text element UI that will display on the game screen. You can also attached a JSON format file and send it to IWM_Update, which will display data on each line after 2 seconds. 


In the project directory, you can run:

### `npm start`
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


# Note

Future build should be saved inside public/ReactTestBuild.

